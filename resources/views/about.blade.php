@extends('layouts.main')

@section('container')
<div class="church-about-area section-space--ptb_120 ">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5">
                <div class="about-tai-content">
                    <p>Meneruskan perjuangan…</p>
                    <p>Ulama yang amat mencintai Allah SWT, Rasulullah SAW</p>
                    <p>dan Al Quran…<p>
                    <div class="section-title-wrap">
                        <h5 class="section-title--two  left-style mb-30" style="font-style: italic">Syeikh Ali Jaber Rahimahullah Ta’ala</h5>
                    </div>
                    <p>Kepergiannya menyisakan asa untuk meneruskan
                        semangatnya menciptakan penghafal Al Quran di bumi Allah
                        SWT dan kemajuan umat Nabi Muhammad SAW </p>

                    <p>Beristirahatlah bersama cahaya Al Qur’an
                        Semoga Allah SWT meridhoi perjuangan kami, aamiin..</p>
                    <h6>Deva Rachman & Ummu Fahad</h6>        
                </div>
            </div>
            <div class="col-lg-7">
                <div class="about-tai-image  small-mb__30 tablet-mb__30">
                    <img src="/images/banners/Syeik Ali Jaber.png" class="img-fluid" alt="About Images">
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- breadcrumb-area end -->
    <div class="row">
        <div class="col-lg-12">
            <div class="section-title-muslim text-center">
                {{-- <h3 class="mb-20">Mosque Foundation</h3> --}}
            </div>
        </div>
    </div>

    <div class="site-wrapper-reveal">

        <div class="church-about-area section-space--ptb_120 ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <div class="about-tai-image  small-mb__30 tablet-mb__30">
                            <img src="/images/banners/Deva Rachman.png" class="img-fluid" alt="About Images">
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="about-tai-content">
                            <div class="section-title-wrap">
                                <h3 class="section-title--two  left-style mb-30">Who is El Medina</h3>
                            </div>
                            <p>representative for Muslim women working to enrich live and humanity, advocating the rights of all women, through authentic leadership based on our Islamic principles </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="section-title-muslim text-center">
                    {{-- <h3 class="mb-20">Mosque Foundation</h3> --}}
                </div>
            </div>
        </div>

        <div class="church-about-area section-space--ptb_120 ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="about-tai-content">
                            <div class="section-title-muslim text-left">
                                <h3 class="mb-20">Personification El Medina Muslimah</h3>
                            </div>
                            <ul style="list-style-type:disc">
                                <li>El medina by name’s meaning is the one who is devoted to faith or civilized</li>
                                <li>Great Muslimah character like Khadijah, Maryam, Aisyah, Fathimah, Sarah</li>
                                <li>Calm, smart, tawakkal</li>
                                <li>Strong, fearless but Allah</li>
                                <li>Caring to herself, religion, family and ummah</li>
                                <li>Always seeking personal growth only for Allah</li>
                            </ul>      
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="about-tai-image  small-mb__30 tablet-mb__30">
                            <img src="/images/banners/5138493.jpg" class="img-fluid" alt="About Images">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="section-title-muslim text-center">
                    {{-- <h3 class="mb-20">Mosque Foundation</h3> --}}
                </div>
            </div>
        </div>

        <!-- ======== Tai About Area Start ========== -->
        <div class="tai-about-area section-space--ptb_120">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <div class="about-muslim-image text-lg-left">
                            <img src="/images/banners/kabah.png" class="img-fluid muslim-image-1" alt="Tai Images">

                            <img src="/images/banners/kaaba-bottom-01.png" class="img-fluid bottom-image-2" alt="Tai Images">
                            <img src="/images/banners/kaaba-bottom-02.png" class="img-fluid bottom-image-3" alt="Tai Images">

                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="about-tai-content small-mt__30 tablet-mt__30">
                            <div class="section-title-muslim text-left">
                                <h3 class="mb-20">Vision and Mission</h3>
                            </div>
                            <h6><strong>Vision:</strong></h6>
                            <ul style="list-style-type:disc">
                                <li>Our vision is to provide Muslim women with opportunities to enable them to actively participate and contribute to
                                    the world's culturally and religiously diverse society.</li>
                                <li>Making the Quran the core of our program with the Quran the ultimate application in life in the world to prepare
                                    for the afterlife</li>   
                            </ul><br>
                            <h6><strong>Mission:</strong></h6>
                            <ul style="list-style-type:disc">
                                <li>Our mission is to empower and connect Muslim women, with a focus on the development of Islamic civilization,
                                    professional development, community engagement and community service and development.</li>
                                <li>Continuing the mission of Sheikh Ali Jaber Rahimahullah in producing generations of qur'ani, memorization of
                                    the qur'an and 1 million memorizations of the qur'an per three years</li>    
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- ======== Tai About Area End ========== -->
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title-muslim text-center">
                    {{-- <h3 class="mb-20">Mosque Foundation</h3> --}}
                </div>
            </div>
        </div>
        
        <!-- ======== Foundation Area Start ========== -->
        <div class="foundation-area section-space--pb_120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title-muslim text-center">
                            <h3 class="mb-20">Our Pillar Programs</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <!-- Foundation Start -->
                        <div class="single-foundation">
                            <div class="foundation-image">
                                <a href="#"><img src="/images/foundation/education.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="foundation-content">
                                <div class="location">
                                    <a href="/education">
                                        <h5>Education</h5>
                                    </a>
                                    <p class="foundation-loction">Well Educated Muslimah</p>
                                </div>
                            </div>
                        </div>
                        <!--// Foundation End -->
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <!-- Foundation Start -->
                        <div class="single-foundation">
                            <div class="foundation-image">
                                <a href="#"><img src="/images/foundation/religious.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="foundation-content">
                                <div class="location">
                                    <a href="#">
                                        <h5>Religious</h5>
                                    </a>
                                    <p class="foundation-loction">Understand and practicing Quran</p>
                                </div>
                            </div>
                        </div>
                        <!--// Foundation End -->
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <!-- Foundation Start -->
                        <div class="single-foundation">
                            <div class="foundation-image">
                                <a href="#"><img src="/images/foundation/economy.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="foundation-content">
                                <div class="location">
                                    <a href="/economy">
                                        <h5>Economy</h5>
                                    </a>
                                    <p class="foundation-loction">Independent Ecnonomy</p>
                                </div>
                            </div>
                        </div>
                        <!--// Foundation End -->
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <!-- Foundation Start -->
                        <div class="single-foundation">
                            <div class="foundation-image">
                                <a href="#"><img src="/images/foundation/personal_growth.png" class="img-fluid" alt=""></a>
                            </div>
                            <div class="foundation-content">
                                <div class="location">
                                    <a href="#">
                                        <h5>Personal Growth</h5>
                                    </a>
                                    <p class="foundation-loction">Great Character</p>
                                </div>
                            </div>
                        </div>
                        <!--// Foundation End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- ======== Foundation Area End ========== -->

        <!-- ======== next foundation ========== -->
        <div class="service-area section-space--pb_120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title-muslim text-center">
                            <h3 class="mb-20">Next Pillar Programs</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <!-- Single Service Start -->
                        <div class="single-service-wrap mt-30">
                            <div class="service-image">
                                <a href="#"><img src="/images/foundation/mastering_quran.png" class="img-fluid" alt="Service image"></a>
                            </div>
                            <div class="service-content">
                                <h4 class="service-title"><a href="#">Mastering & Deep Understanding Quran</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                            </div>
                        </div>
                        <!--// Single Service End -->
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <!-- Single Service Start -->
                        <div class="single-service-wrap mt-30">
                            <div class="service-image">
                                <a href="#"><img src="/images/foundation/oustanding_muslim.png" class="img-fluid" alt="Service image"></a>
                            </div>
                            <div class="service-content">
                                <h4 class="service-title"><a href="#">Outstanding Muslim Economic & Business</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                            </div>
                        </div>
                        <!--// Single Service End -->
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <!-- Single Service Start -->
                        <div class="single-service-wrap mt-30">
                            <div class="service-image">
                                <a href="#"><img src="/images/foundation/halal_gourment.png" class="img-fluid" alt="Service image"></a>
                            </div>
                            <div class="service-content">
                                <h4 class="service-title"><a href="#">Halal Gourmet and Food Store</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                            </div>
                        </div>
                        <!--// Single Service End -->
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <!-- Single Service Start -->
                        <div class="single-service-wrap mt-30">
                            <div class="service-image">
                                <a href="#"><img src="/images/foundation/fun_islamic.png" class="img-fluid" alt="Service image"></a>
                            </div>
                            <div class="service-content">
                                <h4 class="service-title"><a href="#">Fun & Islamic Sports Center</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                            </div>
                        </div>
                        <!--// Single Service End -->
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <!-- Single Service Start -->
                        <div class="single-service-wrap mt-30">
                            <div class="service-image">
                                <a href="#"><img src="/images/foundation/mastering_class.png" class="img-fluid" alt="Service image"></a>
                            </div>
                            <div class="service-content">
                                <h4 class="service-title"><a href="#">Professional Mastering Class</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                            </div>
                        </div>
                        <!--// Single Service End -->
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <!-- Single Service Start -->
                        <div class="single-service-wrap mt-30">
                            <div class="service-image">
                                <a href="#"><img src="/images/foundation/dakwah.png" class="img-fluid" alt="Service image"></a>
                            </div>
                            <div class="service-content">
                                <h4 class="service-title"><a href="#">Dak'wah</a></h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                            </div>
                        </div>
                        <!--// Single Service End -->
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection