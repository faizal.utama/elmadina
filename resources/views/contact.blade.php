@extends('layouts.main')

@section('container')

<div class="site-wrapper-reveal">

    <div class="contact-page-wrapper section-space--pt_120">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="flaticon-placeholder"></i>
                        </div>
                        <div class="contact-info">
                            <h4>Address</h4>
                            <p>Jln Pahlawan Revolusi RT.002/02 No.10 Pondok Bambu <br>
                            Jakarta Timur</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="flaticon-call"></i>
                        </div>
                        <div class="contact-info">
                            <h4>Phone</h4>
                            <p><a href="tel:01234567">021 - 86616576 </a><br>
                                <a href="tel:01234567">0811 88 9119/ 0813 1444 0885</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-contact-info">
                        <div class="contact-icon">
                            <i class="flaticon-paper-plane-1"></i>
                        </div>
                        <div class="contact-info">
                            <h4>Mail</h4>
                            <p><a href="#">admin@elmedina.id</a>
                                </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="contact-form-area section-space--ptb_120">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div id="googleMap-1" class="embed-responsive-item googleMap-1" data-lat="40.730610" data-Long="-73.935242"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact-form-wrap ml-lg-5">
                            <h3 class="title mb-40">Contact Form</h3>
                            <form id="contact-form" action="https://hasthemes.com/file/mail.php" method="post">
                                <div class="contact-form__one">
                                    <div class="contact-input">
                                        <label for="Name">Name</label>
                                        <div class="contact-inner">
                                            <input name="con_name" type="text" placeholder="Enter you name">
                                        </div>
                                    </div>

                                    <div class="contact-input">
                                        <label for="Phone">Phone</label>
                                        <div class="contact-inner">
                                            <input name="con_phone" type="text" placeholder="Your Phone Number">
                                        </div>
                                    </div>

                                    <div class="contact-input">
                                        <label for="Email">Email</label>
                                        <div class="contact-inner">
                                            <input name="con_email" type="email" placeholder="Your Email Address ">
                                        </div>
                                    </div>
                                    <div class="submit-input">
                                        <button class="submit-btn" type="submit">Submit</button>
                                        <p class="form-messege"></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection