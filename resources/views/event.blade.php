@extends('layouts.main')

@section('container')

    <div class="site-wrapper-reveal">
        <div class="donation-area section-space--pb_120 section-space--pt_90">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <!-- Single Donation Wrap Start -->
                        <div class="single-donation-wrap row align-items-center">
                            <div class="col-lg-5">
                                <div class="donation-image">
                                    <img src="images/events/event1.jpeg" class="img-fluid" alt="Donation Image">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="donation-content ml-lg-5">
                                    <div class="donation-title mb-30">
                                        <h4 class="mb-15">TALKSHOW DAN BAZAAR MUSLIMAH EL-MEDINA : MENYAMBUT MUHARRAM 1444H</h4>
                                        <div class="event-date"><span>13 Aug 2022 </span> <span>@10.00am to 03.00pm</span></div>
                                    </div>

                                    <p>Bismillahirrahmanirrahim</p>
                                    <p>Allahumma sholli ala Muhammad</p>
                                    <p>Assalaamu'alaykum warahmatullaahi wabarakaatuh,</P>
                                    <p>Salam silaturahim kami teriring do’a semoga kita selalu senantiasa dalam ridha dan lindungan Allah SWT. Aamiin.</P>
                                    <p>El-Medina Syaikh Ali Jaber Rahimahullah (El-Medina SAJR) hadir menyemangati muslimah Indonesia untuk bersama-sama turut membangun peradaban dunia. El-Medina SAJR adalah majelis yang didirikan oleh Deva Rachman yang melibatkan Ummu Fahad Ali Jaber.</P>
                                    <p>Mari sama-sama kita memulai tahun baru Hijriah dengan thalibul 'ilmi, menuntut ilmu dan berdzikir kepada Allah SWT. Rasulullah SAW bersabda :"Siapa menempuh jalan untuk mencari ilmu, maka Allah akan memudahkan baginya jalan menuju surga"(HR Muslim No.2699). Dan "Barang siapa yang keluar untuk menuntut ilmu, maka ia berada di jalan Allah hingga ia pulang. (HR. Tirmidzi).</p>
                                    <p>Save the date, <strong>15 Muharram 1444H/Sabtu 13 Agustus 2022</strong>, kita akan memulai journey kita mulai pukul 10.00 - 15.00WIB di Masjid Istiqlal Jakarta. InsyaAllah akan ada Beragam kajian ilmu,  diskusi tentang perempuan yang membuka wawasan kita, bagaimana cepat  menghafal Al Quran yang bisa kita terapkan di keluarga kita. Selain itu kita juga akan berzikir muhasabah bersama Ummi Yuni Al Waly, istri dari Almarhum KH Arifin Ilham.</p>
                                    <p><strong>InsyaAllah akan hadir</strong> : Bapak Sandiaga Uno, Ibu Nur Asia Uno, Bapak Prof Dr KH Nasaruddin Umar, Bapak Prof Dr H Arief Rachman MPd, Teteh Khadijah Peggy Melati Sukma, Teteh Cheche Kirani, Teteh Indadari, Ummi Yuni Al Waly, Ummu Fahad Ali Jaber Rahimahullah,  Ustadz Wijayanto, dan lainnya.</p>
                                    <p>Acara ini <strong>bebas biaya (tanpa HTM)</strong>, segera <strong>daftarkan dirimu</strong> di <a href="https://bit.ly/HikmahMuharramElmedina" style="text-decoration: none; color:blue">https://bit.ly/HikmahMuharramElmedina</a></p>
                                    <p>insyaAllah, sampai jumpa di Istiqlal ya Ukhti.. :)</p>
                                    <p>Jazakumullahu khoirul jazaa, Wallahul Muwawiq Ila Aqwamith Thoerieq.</p>
                                    <p>Wassalaamu’alaikum Wr.Wb.</p>            

                                </div>
                            </div>
                        </div>
                        <!--// Single Donation Wrap End -->
                    </div>

                </div>
            </div>
        </div>        

    </div>

@endsection    