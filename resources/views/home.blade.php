@extends('layouts.main')

@section('container')    
    <div class="site-wrapper-reveal no-overflow">

        <div class="site-wrapper-reveal">

            <!-- ======== event Area Start ========== -->
            <div class="donation-area section-space--pb_120 section-space--pt_90">
                <div class="container">
                    <div class="row">
    
                        <div class="col-lg-12">
                            <!-- Single Donation Wrap Start -->
                            <div class="single-donation-wrap row align-items-center">
                                <div class="col-lg-5">
                                    <div class="donation-image">
                                        <img src="images/events/event1.jpeg" class="img-fluid" alt="Donation Image">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="donation-content ml-lg-5">
                                        <div class="donation-title mb-30">
                                            <h4 class="mb-15">TALKSHOW DAN BAZAAR MUSLIMAH EL-MEDINA : MENYAMBUT MUHARRAM 1444H</h4>
                                            <div class="event-date"><span>13 Aug 2022 </span> <span>@10.00am to 03.00pm</span></div>
                                        </div>
    
                                        <p>Bismillahirrahmanirrahim</p>
                                        <p>Allahumma sholli ala Muhammad</p>
                                        <p>Assalaamu'alaykum warahmatullaahi wabarakaatuh,</P>
                                        <p>Salam silaturahim kami teriring do’a semoga kita selalu senantiasa dalam ridha dan lindungan Allah SWT. Aamiin.</P>
                                        <p>El-Medina Syaikh Ali Jaber Rahimahullah (El-Medina SAJR) hadir menyemangati muslimah Indonesia untuk bersama-sama turut membangun peradaban dunia. El-Medina SAJR adalah majelis yang didirikan oleh Deva Rachman yang melibatkan Ummu Fahad Ali Jaber.</P>
                                        <p>Mari sama-sama kita memulai tahun baru Hijriah dengan thalibul 'ilmi, menuntut ilmu dan berdzikir kepada Allah SWT. Rasulullah SAW bersabda :"Siapa menempuh jalan untuk mencari ilmu, maka Allah akan memudahkan baginya jalan menuju surga"(HR Muslim No.2699). Dan "Barang siapa yang keluar untuk menuntut ilmu, maka ia berada di jalan Allah hingga ia pulang. (HR. Tirmidzi).</p>
                                        <p>Save the date, <strong>15 Muharram 1444H/Sabtu 13 Agustus 2022</strong>, kita akan memulai journey kita mulai pukul 10.00 - 15.00WIB di Masjid Istiqlal Jakarta. InsyaAllah akan ada Beragam kajian ilmu,  diskusi tentang perempuan yang membuka wawasan kita, bagaimana cepat  menghafal Al Quran yang bisa kita terapkan di keluarga kita. Selain itu kita juga akan berzikir muhasabah bersama Ummi Yuni Al Waly, istri dari Almarhum KH Arifin Ilham.</p>
                                        <p><strong>InsyaAllah akan hadir</strong> : Bapak Sandiaga Uno, Ibu Nur Asia Uno, Bapak Prof Dr KH Nasaruddin Umar, Bapak Prof Dr H Arief Rachman MPd, Teteh Khadijah Peggy Melati Sukma, Teteh Cheche Kirani, Teteh Indadari, Ummi Yuni Al Waly, Ummu Fahad Ali Jaber Rahimahullah,  Ustadz Wijayanto, dan lainnya.</p>
                                        <p>Acara ini <strong>bebas biaya (tanpa HTM)</strong>, segera <strong>daftarkan dirimu</strong> di <a href="https://bit.ly/HikmahMuharramElmedina" style="text-decoration: none; color:blue">https://bit.ly/HikmahMuharramElmedina</a></p>
                                        <p>insyaAllah, sampai jumpa di Istiqlal ya Ukhti.. :)</p>
                                        <p>Jazakumullahu khoirul jazaa, Wallahul Muwawiq Ila Aqwamith Thoerieq.</p>
                                        <p>Wassalaamu’alaikum Wr.Wb.</p>            
    
                                    </div>
                                </div>
                            </div>
                            <!--// Single Donation Wrap End -->
                        </div>
    
                    </div>
                </div>
            </div>
            <!-- ======== Donation Area End ========== -->
    
            {{-- <!--=========== Causes Details Area Start ==========-->
            <div class="causes-details-area section-space--pb_120">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="mission-wrap mr-lg-5">
                                <div class="section-title-wrap text-left">
                                    <h4 class="section-title-normal mb-30">Event Schedule</h4>
                                </div>
    
                                <div class="target-content">
                                    <ul class="mt-30 mision-list">
                                        <li> > Event Start @ 10:00 am</li>
                                        <li> > Religious Speech @ 9.30 am</li>
                                        <li> > Lecture One @ 10.30 am</li>
                                        <li> > Gest Communication @ 12.00 pm</li>
                                        <li> > Lunch Time @ 1.00 pm</li>
                                        <li> > Ending Program @ 2.00 pm</li>
                                    </ul>
                                </div>
    
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="target-wrap ml-lg-5 small-mt__40 ">
                                <div class="section-title-wrap text-left">
                                    <h4 class="section-title-normal mb-30">Event Venue </h4>
                                </div>
    
                                <div class="target-content">
                                    <ul class="venue-list">
                                        <li><span>Venue 01 </span>
                                            13/b South Convention center
                                            <br>  Dhaka, 1000 Bangladesh.
                                    </li>
                                        <li><span>Venue 02 </span>
                                            577/ed Ipum Road Venu
                                            <br> Demon Ipsum. 15400
                                    </li>
                                    </ul>
                                </div>
    
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="target-wrap ml-lg-5 tablet-mt__40 small-mt__40 ">
                                <div class="section-title-wrap text-left">
                                    <h4 class="section-title-normal mb-30">Event Map </h4>
                                </div>
    
                                <div class="target-content">
                                    <div id="googleMap-1" class="embed-responsive-item googleMap-2" data-lat="40.730610" data-Long="-73.935242"></div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--=========== Causes Details Area End ==========--> --}}
    
        </div>
        
        
        <!-- ======== Service Area End ========== -->

        <!-- ======== Others Activities Area Start ========== -->
        <div class="others-activities-area section-space--pb_120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title-muslim text-center">
                            <h3 class="mb-20">EL-MEDINA Promo</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-12">
                        <!-- Single Activities Start -->
                        <div class="single-activities-wrap">
                            <a href="/umroh" class="activities-imgaes">
                                <img src="/images/events/umroh2.jpeg" class="img-fluid" alt="">
                            </a>
                            
                        </div>
                        <!--// Single Activities End -->
                    </div>
                    <div class="col-lg-4 col-md-6 col-12">
                        <!-- Single Activities Start -->
                        <div class="single-activities-wrap">
                            <a href="/umroh" class="activities-imgaes">
                                <img src="/images/events/umroh1.jpeg" class="img-fluid" alt="">
                            </a>
                            
                        </div>
                        <!--// Single Activities End -->
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- ======== Others Activities Area End ========== -->
    </div>
@endsection    