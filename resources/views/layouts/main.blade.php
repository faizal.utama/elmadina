<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Elmedina.id | {{ $title }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" href="/images/favicon.ico">

    <!-- CSS
        ============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/vendor/bootstrap.min.css">

    <!-- Flaticon Icon CSS -->
    <link rel="stylesheet" href="/css/vendor/flaticon.css">

    <!-- Swiper Slider CSS -->
    <link rel="stylesheet" href="/css/plugins/swiper.min.css">

    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="/css/plugins/magnific-popup.css">


    <!-- Main Style CSS -->
    <link rel="stylesheet" href="/css/style.css">

</head>

<body>





    <!--====================  header area ====================-->
    <div class="header-area header-area--default">


        <!-- Header Bottom Wrap Start -->
        <header class="header-area header-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 d-flex align-items-center">
                        <div class="header__logo">
                            <div class="logo">
                                <a href="/"><img src="/images/logo/logo_elmedina.png" height="150px" width="150px" alt=""></a>
                            </div>
                        </div>
   @include('partials.navbar')                     
                    </div>
                </div>
            </div>
        </header>
        <!-- Header Bottom Wrap End -->

    </div>
    <!--====================  End of header area  ====================-->

@yield('container')

    <!--========== Footer Area Start ==========-->
    <footer class="footer-area bg-footer">
        {{-- <div class="footer-top section-space--ptb_80 section-pb text-white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-footer mt-30">
                            <div class="footer-title">
                                <h6>Address</h6>
                            </div>
                            <div class="footer-contents">
                                <ul>
                                    <li><span>Temple 1: </span> 777/ed Ipum Road Venu
                                        Demon Ipsum. 28400</li>
                                    <li><span>Temple 2: </span> 577/ed Ipum Road Venu
                                        Demon Ipsum. 15400</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <div class="widget-footer mt-30">
                            <div class="footer-title">
                                <h6>Related Links</h6>
                            </div>
                            <div class="footer-contents">
                                <ul>
                                    <li><a href="/">Home</a></li>
                                    <li><a href="/about">About</a></li>
                                    <li><a href="/event">Event</a></li>
                                    <li><a href="/program">Service</a></li>
                                    <li><a href="/contact">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget-footer mt-30">
                            <div class="footer-title">
                                <h6>Information</h6>
                            </div>
                            <div class="footer-contents">
                                <ul>
                                    <li>Sun Rise: 6:00 am</li>
                                    <li>Sun Sat: 5:50 pm</li>
                                    <li>Start Time: 9:00 am</li>
                                    <li>End Time: 11.00 pm</li>
                                    <li>Lunch: 01:30 pm</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="widget-footer mt-30">
                            <div class="footer-title">
                                <h6>Related Links</h6>
                            </div>
                            <div class="footer-logo mb-15">
                                <a href="/"><img src="/images/logo/logo_elmedina.png" alt="" height="120px" width="120px"></a>
                            </div>
                            <div class="footer-contents">
                                <p> Subscribe to our Newsletter & stay update. </p>
                                <div class="newsletter-box">
                                    <input type="text" placeholder="Enter your mail address">
                                    <button><i class="flaticon-paper-plane"></i></button>
                                </div>

                                <ul class="footer-social-share mt-20">
                                    <li><a href="#"><i class="flaticon-facebook"></i></a></li>
                                    <li><a href="#"><i class="flaticon-twitter"></i></a></li>
                                    <li><a href="#"><i class="flaticon-pinterest-social-logo"></i></a></li>
                                    <li><a href="#"><i class="flaticon-youtube"></i></a></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copy-right-box">
                            <p class="text-white">Copyright &copy; 2022 El-Medina <a href="https://elmedina.id/" target="_blank">All Right Reserved</a>.</p>
                            {{-- <p class=" text-white"><a href="/">Privacy policy</a></p> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--==========// Footer Area End ==========-->




    <!--====================  scroll top ====================-->
    <a href="#" class="scroll-top" id="scroll-top">
        <i class="arrow-top flaticon-up-arrow"></i>
        <i class="arrow-bottom flaticon-up-arrow"></i>
    </a>
    <!--====================  End of scroll top  ====================-->


    <!--====================  mobile menu overlay ====================-->
    <div class="mobile-menu-overlay" id="mobile-menu-overlay">
        <div class="mobile-menu-overlay__inner">
            <div class="mobile-menu-overlay__header">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-md-6 col-8">
                            <!-- logo -->
                            <div class="logo">
                                <a href="/">
                                    <img src="/images/logo/logo_elmedina.png" class="img-fluid" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-4">
                            <!-- mobile menu content -->
                            <div class="mobile-menu-content text-right">
                                <span class="mobile-navigation-close-icon" id="mobile-menu-close-trigger"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile-menu-overlay__body">
                <nav class="offcanvas-navigation">
                    <ul>
                        <li class="has-children">
                            <a href="/">Home</a>
                            {{-- <ul class="sub-menu">
                                <li><a href="index.html"><span>Home 1</span></a></li>
                                <li><a href="index-2.html"><span>Home 2</span></a></li>
                                <li><a href="index-3.html"><span>Home 3</span></a></li>
                                <li><a href="index-4.html"><span>Home 4</span></a></li>
                            </ul> --}}
                        </li>
                        <li class="has-children">
                            <a href="/about">about</a>
                        </li>
                        <li class="has-children">
                            <a href="/event">Event</a>
                        </li>
                        <li class="has-children">
                            <a href="/program">Program</a>
                        </li>
                        <li class="has-children">
                            <a href="/contact">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!--====================  End of mobile menu overlay  ====================-->





    <!-- JS
    ============================================ -->

    <!-- Modernizer JS -->
    <script src="/js/vendor/modernizr-2.8.3.min.js"></script>

    <!-- jquery JS -->
    <script src="/js/vendor/jquery-3.5.1.min.js"></script>
    <script src="/js/vendor/jquery-migrate-3.3.0.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="/js/vendor/bootstrap.min.js"></script>

    <!-- Swiper Slider JS -->
    <script src="/js/plugins/swiper.min.js"></script>

    <!-- Waypoints JS -->
    <script src="/js/plugins/waypoints.min.js"></script>

    <!-- Counterup JS -->
    <script src="/js/plugins/counterup.min.js"></script>

    <!-- Magnific Popup JS -->
    <script src="/js/plugins/jquery.magnific-popup.min.js"></script>

    <!-- wow JS -->
    <script src="/js/plugins/wow.min.js"></script>

    <!-- Ajax mail JS -->
    <script src="/js/plugins/ajax.mail.js"></script>

    <!-- Plugins JS (Please remove the comment from below plugins.min.js for better website load performance and remove plugin js files from avobe) -->

    <!--
    <script src="/js/plugins/plugins.min.js"></script>
    -->

    <!-- Main JS -->
    <script src="/js/main.js"></script>


</body>

</html>