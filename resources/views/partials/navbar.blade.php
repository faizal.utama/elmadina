<div class="header-right">
    <div class="header__navigation menu-style-three d-none d-lg-block">
        <nav class="navigation-menu">
            <ul>
                <li class="has-children has-children--multilevel-submenu active">
                    <a href="/"><span>Home</span></a>
                </li>
                <li class="has-children">
                    <a href="/about"><span>About</span></a>
                </li>
                <li class="has-children">
                    <a href="/event"><span>Event</span></a>
                </li>
                <li class="has-children has-children--multilevel-submenu">
                    <a href="/program"><span>Program</span></a>
                    <!-- <ul class="submenu">
                        <li><a href="it-services.html"><span>IT Services</span></a></li>
                    </ul> -->
                </li>
                {{-- <li class="has-children has-children--multilevel-submenu">
                    <a href="donation.html"><span>Causes</span></a>
                    <ul class="submenu">
                        <li><a href="causes.html"><span>Causes</span></a></li>
                        <li><a href="gallery.html"><span>Gallery</span></a></li>
                        <li><a href="mission-and-vision.html"><span>Mission & Vision</span></a></li>
                        <li><a href="causes-details.html"><span>Causes Details</span></a></li>
                        <li><a href="events-details.html"><span>Events Details</span></a></li>
                    </ul>
                </li> --}}
                <li class="has-children">
                    <a href="/contact"><span>Contact</span></a>
                </li>

            </ul>
        </nav>

    </div>

    {{-- <div class="header-btn text-right d-none d-sm-block ml-lg-4">
        <a class="btn-circle btn-default btn" href="#">Donate</a>
    </div> --}}

    <!-- mobile menu -->
    <div class="mobile-navigation-icon d-block d-lg-none" id="mobile-menu-trigger">
        <i></i>
    </div>
</div>