@extends('layouts.main')

@section('container')

<div class="site-wrapper-reveal">

    <!-- ======== Service Area Start ========== -->
    <div class="foundation-area section-space--pb_120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-muslim text-center">
                        <h3 class="mb-20 mt-20">Our Pillar Programs</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <!-- Foundation Start -->
                    <div class="single-foundation">
                        <div class="foundation-image">
                            <a href="#"><img src="/images/foundation/education.png" class="img-fluid" alt=""></a>
                        </div>
                        <div class="foundation-content">
                            <div class="location">
                                <a href="/education">
                                    <h5>Education</h5>
                                </a>
                                <p class="foundation-loction">Well Educated Muslimah</p>
                            </div>
                        </div>
                    </div>
                    <!--// Foundation End -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <!-- Foundation Start -->
                    <div class="single-foundation">
                        <div class="foundation-image">
                            <a href="#"><img src="/images/foundation/religious.png" class="img-fluid" alt=""></a>
                        </div>
                        <div class="foundation-content">
                            <div class="location">
                                <a href="#">
                                    <h5>Religious</h5>
                                </a>
                                <p class="foundation-loction">Understand and practicing Quran</p>
                            </div>
                        </div>
                    </div>
                    <!--// Foundation End -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <!-- Foundation Start -->
                    <div class="single-foundation">
                        <div class="foundation-image">
                            <a href="#"><img src="/images/foundation/economy.png" class="img-fluid" alt=""></a>
                        </div>
                        <div class="foundation-content">
                            <div class="location">
                                <a href="/economy">
                                    <h5>Economy</h5>
                                </a>
                                <p class="foundation-loction">Independent Ecnonomy</p>
                            </div>
                        </div>
                    </div>
                    <!--// Foundation End -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <!-- Foundation Start -->
                    <div class="single-foundation">
                        <div class="foundation-image">
                            <a href="#"><img src="/images/foundation/personal_growth.png" class="img-fluid" alt=""></a>
                        </div>
                        <div class="foundation-content">
                            <div class="location">
                                <a href="#">
                                    <h5>Personal Growth</h5>
                                </a>
                                <p class="foundation-loction">Great Character</p>
                            </div>
                        </div>
                    </div>
                    <!--// Foundation End -->
                </div>
            </div>
        </div>
    </div>
    <!-- ======== Service Area End ========== -->

    <div class="service-area section-space--pb_120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-muslim text-center">
                        <h3 class="mb-20">Next Pillar Programs</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <!-- Single Service Start -->
                    <div class="single-service-wrap mt-30">
                        <div class="service-image">
                            <a href="#"><img src="/images/foundation/mastering_quran.png" class="img-fluid" alt="Service image"></a>
                        </div>
                        <div class="service-content">
                            <h4 class="service-title"><a href="#">Mastering & Deep Understanding Quran</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                        </div>
                    </div>
                    <!--// Single Service End -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <!-- Single Service Start -->
                    <div class="single-service-wrap mt-30">
                        <div class="service-image">
                            <a href="#"><img src="/images/foundation/oustanding_muslim.png" class="img-fluid" alt="Service image"></a>
                        </div>
                        <div class="service-content">
                            <h4 class="service-title"><a href="#">Outstanding Muslim Economic & Business</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                        </div>
                    </div>
                    <!--// Single Service End -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <!-- Single Service Start -->
                    <div class="single-service-wrap mt-30">
                        <div class="service-image">
                            <a href="#"><img src="/images/foundation/halal_gourment.png" class="img-fluid" alt="Service image"></a>
                        </div>
                        <div class="service-content">
                            <h4 class="service-title"><a href="#">Halal Gourmet and Food Store</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                        </div>
                    </div>
                    <!--// Single Service End -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <!-- Single Service Start -->
                    <div class="single-service-wrap mt-30">
                        <div class="service-image">
                            <a href="#"><img src="/images/foundation/fun_islamic.png" class="img-fluid" alt="Service image"></a>
                        </div>
                        <div class="service-content">
                            <h4 class="service-title"><a href="#">Fun & Islamic Sports Center</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                        </div>
                    </div>
                    <!--// Single Service End -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <!-- Single Service Start -->
                    <div class="single-service-wrap mt-30">
                        <div class="service-image">
                            <a href="#"><img src="/images/foundation/mastering_class.png" class="img-fluid" alt="Service image"></a>
                        </div>
                        <div class="service-content">
                            <h4 class="service-title"><a href="#">Professional Mastering Class</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                        </div>
                    </div>
                    <!--// Single Service End -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <!-- Single Service Start -->
                    <div class="single-service-wrap mt-30">
                        <div class="service-image">
                            <a href="#"><img src="/images/foundation/dakwah.png" class="img-fluid" alt="Service image"></a>
                        </div>
                        <div class="service-content">
                            <h4 class="service-title"><a href="#">Dak'wah</a></h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been standard dummy text.</p>
                        </div>
                    </div>
                    <!--// Single Service End -->
                </div>
            </div>
        </div>
    </div>

    <!--activity-->
    <div class="foundation-area section-space--pb_120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-muslim text-center">
                        <h3 class="mb-20">El-Medina Activities</h3>
                    </div>                  
                </div>
            </div>
            <div class="row">
                <h4 class="mb-15">Routine Activity</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                          <th scope="col">Current</th>
                          <th scope="col">Next Project</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Khataman ba’da shubuh</td>
                          <td>BASIC KNOWLEDGE FOR MUALLAF</td>
                        </tr>
                        <tr>
                            <td>TAHSIN FOR BASIC, INTERMEDIATE, ADVANCE</td>
                            <td>HAFIDZ – HAFALAN ALQURAN
                                GENERAL CLASS
                                EXECUTIVE CLASS</td>
                          </tr>
                          <tr>
                            <td>TAKLIM
                                SUNDAY MORNING
                                MONTHLY (TOPIC: WOMEN, PARENTING,
                                GENERAL)
                                QUARTERLY – OFFLINE</td>
                            <td></td>
                          </tr>
                      </tbody>
                  </table>
            </div>

            <div class="row">
                <h4 class="mb-15 mt-20">Health And Food</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                          <th scope="col">SPORT CLASS</th>
                          <th scope="col">HEALTHY BOOSTER</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>HIJABER GYM</td>
                          <td>ARAB FRUIT – QURMA, TIN, ETC</td>
                        </tr>
                        <tr>
                            <td>MUAY THAI</td>
                            <td>HABBATS, MADU KURMA, OTHERS</td>
                          </tr>
                          <tr>
                            <td>JOGGING</td>
                            <td>HEALTHY JUICE CORNER</td>
                          </tr>
                          <tr>
                            <td>TRACKING – TADABUR ALAM</td>
                            <td></td>
                          </tr>
                      </tbody>
                  </table>
            </div>

            <div class="row">
                <h4 class="mb-15 mt-20">Business</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                          <th scope="col">BUSINESS EXPANSION</th>
                          <th scope="col">TRAINING</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>STALL/BAZAAR</td>
                          <td>ENTERPRENEUR SKILL</td>
                        </tr>
                        <tr>
                            <td>E-COMMERCE</td>
                            <td>FRANCHISE TRAINING</td>
                          </tr>
                          <tr>
                            <td>PRODUCT DEMO</td>
                            <td>COST AND PRODUCTION TRAINING</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>CASH FLOW TRAINING – Pembukuan,
                                penggajian, dll</td>
                          </tr>
                      </tbody>
                  </table>
            </div>
        </div>
    </div>           

</div>
@endsection